#!/usr/bin/perl
use strict;
use warnings;

use Getopt::Long;

use lib $ENV{GL_LIBDIR};
use Gitolite::Rc;
use Gitolite::Common;

# To parse the pubkeyfile with options etc.
# 0.16 adds ecdsa keys
# 0.17 adds ed25519 keys
# 0.17.xx Gentoo patching adds security keys
use Net::SSH::AuthorizedKeysFile 0.17;

$|++;

# best called via 'gitolite trigger POST_COMPILE'; other modes at your own
# risk, especially if the rc file specifies arguments for it.  (That is also
# why it doesn't respond to "-h" like most gitolite commands do).

# option procesing
# ----------------------------------------------------------------------

# currently has one option:
#   -kfn, --key-file-name        adds the keyfilename as a second argument

my $kfn = '';
GetOptions( 'key-file-name|kfn' => \$kfn, );

tsh_try("sestatus");
my $selinux = ( tsh_text() =~ /enforcing/ );

my $ab = $rc{GL_ADMIN_BASE};
trace( 1, "'keydir' not found in '$ab'; exiting" ), exit if not -d "$ab/keydir";
my $akdir        = "$ENV{HOME}/.ssh";
my $akfile       = "$ENV{HOME}/.ssh/authorized_keys";
my $glshell      = $rc{GL_BINDIR} . "/gitolite-shell";
my $auth_options = auth_options();
my $auth_options_preserve = auth_options_preserve();

sanity();

# ----------------------------------------------------------------------

_chdir($ab);

# old data
my $old_ak = slurp($akfile);
my @non_gl = grep { not /^# gito.*start/ .. /^# gito.*end/ } slurp($akfile);
chomp(@non_gl);
my %seen = map { $_ => 'a non-gitolite key' } ( fp(@non_gl) );

# pubkey files
chomp( my @pubkeys = `find keydir/ -type f -name "*.pub" | sort` );
my @gl_keys = ();
for my $f (@pubkeys) {
    my $fp = fp($f);
    if ( $seen{$fp} ) {
        _warn "$f duplicates $seen{$fp}, sshd will ignore it";
    } else {
        $seen{$fp} = $f;
    }
	@gl_keys = (@gl_keys, optionise_gentoo($f));
}

# dump it out
my $out = join( "\n", @non_gl, "# gitolite start", @gl_keys, "# gitolite end" ) . "\n";

my $ak = slurp($akfile);
_die "'$akfile' changed between start and end of this program!" if $ak ne $old_ak;
_print( $akfile, $out );

_warn "you have no keys left; I hope you intended to do that!" unless @gl_keys;

# ----------------------------------------------------------------------

sub sanity {
    _die "'$glshell' not found; this should NOT happen..."                if not -f $glshell;
    _die "'$glshell' found but not readable; this should NOT happen..."   if not -r $glshell;
    _die "'$glshell' found but not executable; this should NOT happen..." if not -x $glshell;

    my $n = "    (this is normal on a brand new install)";
    _warn "$akdir missing; creating a new one\n$n"  if not -d $akdir;
    _warn "$akfile missing; creating a new one\n$n" if not -f $akfile;

    _mkdir( $akdir, 0700 ) if not -d $akdir;
    if ( not -f $akfile ) {
        _print( $akfile, "" );
        chmod 0600, $akfile;
    }
}

sub auth_options {
    my $auth_options = $rc{AUTH_OPTIONS};
    $auth_options ||= "no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty";

    return $auth_options;
}

sub auth_options_preserve {
	my $auth_options_preserve = $rc{AUTH_OPTIONS_PRESERVE};
	$auth_options_preserve ||= "from";

	return $auth_options_preserve;
}

sub fp {
    # input: see below
    # output: a (list of) FPs
    my $in = shift || '';
    if ( $in =~ /\.pub$/ ) {
        # single pubkey file
        _die "bad pubkey file '$in'" unless $in =~ $REPONAME_PATT;
        return fp_file($in);
    } elsif ( -f $in ) {
        # an authkeys file
        return map { fp_line($_) } grep { !/^#/ and /\S/ } slurp($in);
    } else {
        # one or more actual keys
        return map { fp_line($_) } grep { !/^#/ and /\S/ } ( $in, @_ );
    }
}

sub fp_file {
    return $selinux++ if $selinux;    # return a unique "fingerprint" to prevent noise
    my $f  = shift;
    my ($fp, $output) = ssh_fingerprint_file($f);
    _die "fingerprinting failed for '$f': $output" unless $fp;
    return $fp;
}

sub fp_line {
    my $line = shift;
    my ($fp, $output) = ssh_fingerprint_line($line);
    _die "fingerprinting failed for '$line': $output" unless $fp;
    return $fp;
}

# NOTE: optionise() is not used in gitolite-gentoo
sub optionise {
    my $f = shift;

    my $user = $f;
    $user =~ s(.*/)();                # foo/bar/baz.pub -> baz.pub
    $user =~ s/(\@[^.]+)?\.pub$//;    # baz.pub, baz@home.pub -> baz

    my @line = slurp($f);
    if ( @line != 1 ) {
        _warn "$f does not contain exactly 1 line; ignoring";
        return '';
    }
    chomp(@line);
    return "command=\"$glshell $user" . ( $kfn ? " $f" : "" ) . "\",$auth_options $line[0]";
}

sub optionise_gentoo {
	my $f = shift;

	my $user = $f;
	$user =~ s(.*/)();                # foo/bar/baz.pub -> baz.pub
	$user =~ s/(\@[^.]+)?\.pub$//;    # baz.pub, baz@home.pub -> baz

	# Metadata check
	if(exists($rc{"GL_METADATA_REQUIRED"}) && @{$rc{"GL_METADATA_REQUIRED"}} gt 0) {
		my @not_met = @{$rc{"GL_METADATA_REQUIRED"}};
		my $pubkey = _open('<', $f);
		while(defined(my $line = <$pubkey>)) {
			chomp($line);
			next if $line !~ m/^\s*#\s*[-a-zA-Z0-9_]+:/;
			$line =~ s/^\s*#\s*//;

			my ($variable, $value) = split(/:\s*/, $line, 2);

			if(grep(/^\Q${variable}\E$/, @{$rc{"GL_METADATA_REQUIRED"}})) {
				if(length($value) > 0) {
					@not_met = grep(!/^\Q${variable}\E$/, @not_met);
				}
			}
		}
		close($pubkey);

		if( $#not_met ne -1 ) {
			_warn "Skipping '${f}' due to missed required variables: ".join(", ", sort @not_met);
			return ();
		}
	}

	# Use strict mode to abort on faulty files
	my $akf = Net::SSH::AuthorizedKeysFile->new( strict => 1, );
	$akf->read($f);

	my @keys = ();

	foreach my $keyobj ($akf->keys()) {
		if(!defined($keyobj)) {
			_warn "Malformed key '${f}', skipping...\n";
			next;
		}

		# Preserve only options specified in AUTH_OPTIONS_PRESERVE
		foreach my $option (keys(%{$keyobj->options})) {
			if(!grep(/^\Q${option}\E$/, split(",", $auth_options_preserve))) {
				delete($keyobj->options->{$option});
			}
		}

		# Add our options as well
		foreach my $option (split(",", $auth_options)) {
			$keyobj->option($option, 1);
		}

		$keyobj->option("command",  "${glshell} ${user}" . ( $kfn ? " $f" : "" ));

		push(@keys, $keyobj->as_string());
	}

	return @keys;
}
