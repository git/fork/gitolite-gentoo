#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;
use Data::Dumper;

# this is hardcoded; change it if needed
use lib "$ENV{PWD}/src/lib";
use Gitolite::Common;
use Gitolite::Test;
use Gitolite::Rc;
use Gitolite::Conf::Load;

BEGIN {
    $ENV{G3T_RC} = "$ENV{HOME}/g3trc";
    put "$ENV{G3T_RC}", "";
}

my $bd = `gitolite query-rc -n GL_BINDIR`;
my $h  = $ENV{HOME};
my $ab = `gitolite query-rc -n GL_ADMIN_BASE`;
my $ak = "$ENV{HOME}/.ssh/authorized_keys";
my $kd = `gitolite query-rc -n GL_ADMIN_BASE` . "/keydir";
umask 0077;

# test metadata in keyfiles
# ----------------------------------------------------------------------
confreset; confadd '
    @g1 = u1
    @g2 = u2
    repo foo
        RW = @g1 u3
        R  = @g2 u4
';


# This is a special command to test that metadata is exporter to the
# environment for hooks/commands to use.
my $printenv_cmd = $bd.'/commands/printenv.t';

open(FH, '>>', $ENV{HOME}.'/.gitolite.rc');

print FH <<"EOF";
\$RC{GL_METADATA} = [ 'glt-meta-required', 'glt-meta-optional', 'glt-meta-append' ];
\$RC{GL_METADATA_REQUIRED} = [ 'glt-meta-required' ];
\$RC{GL_METADATA_APPENDED} = [ 'glt-meta-append' ];
push \@{ \$RC{ENABLE} }, "printenv.t";

# Required as last line.
1;
EOF

close FH;

put $printenv_cmd, <<'EOF';
#!/bin/sh
#printenv -0 |grep --null-data -i -e gl -e glt -e gitolite |sort -z |tr '\0' '\n'
printenv  |grep -i -e gl -e glt -e gitolite |sort
EOF
chmod 0755, $printenv_cmd;

END {
	unlink $printenv_cmd;
}

try "
    plan 49;

    grep printenv $printenv_cmd;    ok or die 8;

    # reset stuff
    rm -f $h/.ssh/authorized_keys;          ok or die 1

    cp $bd/../t/keys/u[1-6]* $h/.ssh;       ok or die 2
    cp $bd/../t/keys/admin*  $h/.ssh;       ok or die 3
    cp $bd/../t/keys/config  $h/.ssh;       ok or die 4
        cat $h/.ssh/config
        perl s/%USER/$ENV{USER}/
        put $h/.ssh/config

    mkdir                  $kd/;      ok or die 5
    cp $bd/../t/keys/*.pub $kd/;      ok or die 6

    # Setup authorized_keys with third parameter for keyfiles names, and validates the metadata.
    gitolite ../triggers/post-compile/ssh-authkeys --key-file-name;  ok or die 7;

    ssh u1 printenv.t;                ok;   /glt_meta/
                                            /glt_meta_required=u1_req/
                                            /glt_meta_optional=u1_opt/
                                            !/glt_meta_required=u2_req/
                                            !/glt_meta_optional=u2_opt/
                                            /glt_meta_append=u1.entry1 u1.entry2/
                                            !/glt_meta_append=u1.entry1 u1.entry2./

    ssh u2 printenv.t;                ok;   /glt_meta/
                                            !/glt_meta_required=u1_req/
                                            !/glt_meta_optional=u1_opt/
                                            /glt_meta_required=u2_req/
                                            /glt_meta_optional=u2_opt/

    ## Set u1 key to be missing required metadata
    cat $kd/u1.pub
    perl s/glt/xglt/g
    put $kd/u1.pub

    # Should *omit* the u1 key
    gitolite ../triggers/post-compile/ssh-authkeys --key-file-name;  ok or die 8;
	grep keydir/u1.pub $ak;				!ok;	!/opt.u1/

    ## Set u1 key to be have metadata key conflicts
    cat $bd/../t/keys/u1.pub							; ok
    put $kd/u1.pub										; ok
	echo '# glt-meta-optional: xxconflict' >>$kd/u1.pub	; ok

    # Should ssh-authkeys should WORK, NON-FATAL
    gitolite ../triggers/post-compile/ssh-authkeys --key-file-name;  ok or die 9;
	# But this should fail with the conflict
    ssh u1 printenv.t;                !ok;  /Metadata glt-meta-optional has conflicted values:/
											/glt-meta-optional.*u1_opt/
											/glt-meta-optional.*xxconflict/

	# Repair key.
    cp $bd/../t/keys/*.pub $kd/;      ok or die 10

    # Setup authorized_keys with scan for keyfile based on user.
    gitolite ../triggers/post-compile/ssh-authkeys;					 ok or die 11;

    ssh u1 printenv.t;                ok;   /glt_meta/
                                            /glt_meta_required=u1_req/
                                            /glt_meta_optional=u1_opt/
                                            !/glt_meta_required=u2_req/
                                            !/glt_meta_optional=u2_opt/
                                            /glt_meta_append=u1.entry1 u1.entry2/
                                            !/glt_meta_append=u1.entry1 u1.entry2./

    ssh u2 printenv.t;                ok;   /glt_meta/
                                            !/glt_meta_required=u1_req/
                                            !/glt_meta_optional=u1_opt/
                                            /glt_meta_required=u2_req/
                                            /glt_meta_optional=u2_opt/
";
