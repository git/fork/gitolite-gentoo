#!/bin/bash

branch=$(git rev-parse --abbrev-ref HEAD)

if [ "${branch}" = "master-g2" ]; then
	# GL2
	export GL_BINDIR="$(pwd)/src"
	export GL_RC="$(pwd)/conf/example.gitolite.rc"

	dirs="conf/ hooks/ src/"
else
	# GL3
	export GL_LIBDIR="$(pwd)/src"

	dirs="src/"
	perl_opts="-Isrc/lib/"
fi

if [ -z "${1}" ]; then
	tag=$(git tag | grep '^gitolite-gentoo-.*$' | sort -Vr | head -n 1)
else
	tag=$1
fi

for foo in $(git ls-files ${dirs} ); do
	[ "${foo:$((${#foo}-3))}" = ".pm" ] && { perl $perl_opts -c $foo; continue; }
	[ -n "$(grep -m 1 '^#!/usr/bin/perl' $foo)" ] && { perl $perl_opts -c $foo; continue; }
done

git archive --prefix=${tag}/ --format tar $branch > ${tag}.tar
bzip2 -9 ${tag}.tar
